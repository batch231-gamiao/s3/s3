// console.log("hello");

// // To create a class in JS, we use the class keyword and {}
// naming convention for classes begins with uppercase characters
/*
	Syntax:
	class ClassName {
		properties
	};
*/

class Dog {
	// we add constructor method to a class to be able to initialize values upon instantiation of an object from a class
	constructor(name, breed, age){

		this.name = name;
		this.breed = breed;
		this.age = age * 7;
	}
}

// instantiation - process of creating objects
// an object created class is called an instance
// new keyword is used to instantiate an object from a class.
let dog1 = new Dog("Puchi", "golden retriever", 0.6);
console.log(dog1);

/*
	mini-activity: (10 mins)
	Create a new class called Person.

	This Person class should be able to instantiate a new object with the folowing fields:

	name,
	age,
	nationality,
	address.

	Insantiate 2 new objects from the Person class as person1 and perso2.

	Log both objects in the console. Take a screenshot of your console and send it to the hangouts.

*/

// SOLUTION:

class Person {
	constructor(name,age,nationality,address){
		this.name = name;
		if(age >= 18) {
			this.age = age;
		}
		else {
			this.age = undefined;
		}
		this.nationality = nationality;
		this.address = address;
	}

	// Activity Solution
	// 1.
		// a.
		greet() {
			console.log("Hello! Good Morning!");
			return this;
		}

		// b.
		introduce() {
			console.log(`Hi! My name is ${this.name}`)
			return this;
		}

		// c.
		changeAddress(value) {
			this.address = value;
			console.log(`${this.name} now lives in ${this.address}`)
			return this;
		}
	// 2. All methods are chainable

	// 3. Refactor age

}

let person1 = new Person("Joshua", 22, "Filipino", "Laguna");
console.log(person1);

let person2 = new Person("Steve Gates", 22, "Japanese", "Marikina");
console.log(person2);

/*
	Mini-Quiz: (10 mins)
		1. What is the blueprint where objects are created from?
	        Answer: Class


	    2. What is the naming convention applied to classes?
	        Answer: Uppercase


	    3. What keyword do we use to create objects from a class?
	        Answer: new


	    4. What is the technical term for creating an object from a class?
	        Answer: instantiation


	    5. What class method dictates HOW objects will be created from that class?
	    	Answer: constructor
*/

// class student

class Student {
	constructor(name, email, grades){
		this.name = name;
		this.email = email;

		// check if first the array has 4 elements
		if(grades.length === 4){
			if(grades.every(grade => grade >= 0 && grade <= 100)){
				this.grades = grades;
			}
			else {
				this.grades = undefined;
			}
		}
		else {
			this.grades = undefined;
		}

		this.gradeAve = undefined;
		this.passed = undefined;
		this.passedWithHonors = undefined;
	}
	// class methods = would be common to all instances.
	// make sure they are NOT separated by comma.
	login() {
		console.log(`${this.email} has logged in`);
		return this;
	}

	logout(email){
    	console.log(`${this.email} has logged out`);
    	return this;
 	}

 	listGrades(grades){
 	    this.grades.forEach(grade => {
 	        console.log(`${this.name}'s quarterly grade averages are: ${grade}`);
 	    })
 	    return this;
	}

	computeAve(){
		let sum = 0;
		this.grades.forEach(grade=> sum = sum + grade);
		this.gradeAve = sum/4;

		return this;
	}

	willPass() {
		this.passed = this.computeAve().gradeAve >= 85 ? true : false;
		return this;
	}

	willPassWithHonors(){
		if(this.passed){
			if(this.gradeAve >= 90){
				this.passedWithHonors = true;
			}
			else {
				this.passedWithHonors = false;
			}
		}
		else {
			this.passedWithHonors = false;
		}
		return this;
	} 
}

// instantiate all four students from s2 using Student class
let studentOne = new Student("Tony", "starksindustries@mail.com", [89,84,78,88]);
let studentTwo = new Student("Peter", "spideyman@mail.com", [78,82,79,85]);
let studentThree = new Student("Wanda", "scarlettMaximoff@mail.com", [87,89,91,93]);
let studentFour = new Student("Steve", "captainRogers@mail.com", [91,89,92,93]);

/*
	Activity #3:
     1. Should class methods be included in the class constructor?
        Answer: No


    2. Can class methods be separated by commas?
        Answer: No


    3. Can we update an object’s properties via dot notation?
        Answer: yes


    4. What do you call the methods used to regulate access to an object’s properties?
        Answer: class methods


    5. What does a method need to return in order for it to be chainable?
        Answer: the "this" keyword
*/

/*
	Function Coding Activity: (45 mins)

	1. Modify the class Person and add the following methods:
	    a. greet method- the person should be able to greet a certain message
	    b. introduce method- person should be be able to state their own name
	    c. change address- person should be able to update their address and send a message where he now lives
	2. All methods should be chainable.
	3. Refactor the age by having a condition in which the age should only take in a number data type and a legal age of 18, otherwise undefined
	4. Send a screenshot of your outputs in Hangouts and link it in Boodle
*/

// 1.

